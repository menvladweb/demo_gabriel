import 'package:flutter/material.dart';

class Item {
  final String image;
  final String dateTime;
  final String lang;
  final String lot;

  Item({@required this.image, @required this.dateTime, @required this.lang, @required this.lot});

  Map<String, dynamic> toMap() {
    return {
      'image': image,
      'dateTime': dateTime,
      'lang': lang,
      'lot': lot,
    };
  }
}
