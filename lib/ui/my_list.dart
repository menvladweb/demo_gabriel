import 'dart:convert';
import 'dart:io';

import 'package:camera/camera.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gabriel/model/item_list.dart';
import 'package:flutter_gabriel/ui/take_camera.dart';
import 'package:flutter_gabriel/variebles/varieble.dart';
import 'package:geolocator/geolocator.dart';
import 'package:sqflite/sqflite.dart';

List<CameraDescription> cameras;
List<Map<String, dynamic>> maps;
List<Item> listItem;
bool serviceEnabled;
LocationPermission permission;

class MyList extends StatefulWidget {
  @override
  _MyListState createState() => _MyListState();
}

class _MyListState extends State<MyList> {
  @override
  void initState() {
    getDataBase();
    _determinePosition();
    super.initState();
  }

  Future<void> getDataBase() async {
    // open the database
    await openDatabase('demo.db', version: 1, onCreate: (Database db, int version) async {
      // When creating the db, create the table
      await db.execute('CREATE TABLE item (id INTEGER PRIMARY KEY, image TEXT, date TEXT, lang TEXT, lot TEXT)');
    });

    await _getDataBase();
  }

  Future<Position> _determinePosition() async {
    // Test if location services are enabled.
    serviceEnabled = await Geolocator.isLocationServiceEnabled();
    if (!serviceEnabled) {
      // Location services are not enabled don't continue
      // accessing the position and request users of the
      // App to enable the location services.
      return Future.error('Location services are disabled.');
    }



    permission = await Geolocator.checkPermission();
    if (permission == LocationPermission.denied) {
      permission = await Geolocator.requestPermission();
      if (permission == LocationPermission.denied) {
        // Permissions are denied, next time you could try
        // requesting permissions again (this is also where
        // Android's shouldShowRequestPermissionRationale
        // returned true. According to Android guidelines
        // your App should show an explanatory UI now.
        return Future.error('Location permissions are denied');
      }
    }


    if (permission == LocationPermission.deniedForever) {
      // Permissions are denied forever, handle appropriately.
      return Future.error('Location permissions are permanently denied, we cannot request permissions.');
    }
    return await Geolocator.getCurrentPosition();
  }

  Future<void> _getDataBase() async {
    var db = await openDatabase('demo.db');
    maps = await db.query('item');

    setState(() {
      listItem = List.generate(maps.length, (i) {
        return Item(
          image: maps[i]['image'],
          dateTime: maps[i]['date'],
          lang: maps[i]['lang'],
          lot: maps[i]['lot'],
        );
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(title),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.my_library_add),
            color: Colors.white,
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => CameraScreen(),
                ),
              ).then((value) => {_getDataBase()});
            },
          ),
        ],
      ),
      body: listItem != null
          ? listItem.length > 0
              ? _myListView()
              : _circleText(context)
          : _circleLoad(context),
    );
  }
}

Widget _circleText(context) {
  return Center(
      child: Text(
    'No item have',
    style: TextStyle(color: Colors.black, fontSize: 16),
  ));
}

Widget _circleLoad(context) {
  return Center(
      child: SizedBox(
    height: MediaQuery.of(context).size.height / 1.3,
    child: Center(
      child: CircularProgressIndicator(
        valueColor: AlwaysStoppedAnimation<Color>(Colors.green),
      ),
    ),
  ));
}

Widget _myListView() {
  return ListView.builder(
      itemCount: listItem.length,
      // ignore: missing_return
      itemBuilder: (context, index) {
        // ignore: missing_return
        final item = listItem[index];

        return _myListItemWidget(item);
      });
}

Widget _myListItemWidget(item) {
  return Card(
    clipBehavior: Clip.antiAlias,
    child: Container(
      height: 120,
      padding: const EdgeInsets.all(0),
      child: Row(children: [
        Expanded(
          flex: 6,
          child: getImagenBase64(item.image),
        ),
        Spacer(
          flex: 1,
        ),
        Expanded(
          flex: 10,
          child: Container(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  'Date : ',
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
                Text(
                  item.dateTime.toString(),
                  style: TextStyle(fontSize: 16.0),
                ),
                Text(
                  'Location : ',
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
                Text(
                  item.lot.toString() + ' ' + item.lang.toString(),
                  style: TextStyle(fontSize: 16.0),
                ),
              ],
            ),
          ),
        ),
      ]),
    ),
  );
}

Widget getImagenBase64(String imagen) {
  String _imageBase64 = imagen;
  const Base64Codec base64 = Base64Codec();
  if (_imageBase64 == null) return new Container();
  final bytes = base64.decode(_imageBase64);
  return Image.memory(
    bytes,
    fit: BoxFit.fitWidth,
  );
}
